﻿using UnityEngine;
using System.Collections;

public class PropellerRotation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {	
		this.transform.Rotate(Vector3.back * Time.deltaTime*1500.0f);
	
	}
}
