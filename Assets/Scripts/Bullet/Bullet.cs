﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	// fly speed
	public float speed = 10.0f;
	public float damage = 1;
	public  int damage2 = 1;
	public bool slow = false;
	public bool poison = false;
	
	// destination set by Tower when creating the bullet
	Transform destination;    
	
	// Update is called once per frame
	void Update () {
		// destroy bullet if destination does not exist anymore
		if (destination == null) {
			Destroy(gameObject);
			return;
		}
		
		// fly towards the destination
		float stepSize = Time.deltaTime * speed;
		transform.position = Vector3.MoveTowards(transform.position, destination.position, stepSize);
		
		// reached?
		if (transform.position.Equals(destination.position)) {
			// decrease teddy health
			Enemy_one t = destination.GetComponent<Enemy_one>();
			if(slow == true){
				t.slowme();
				//SleepTimeout(5000);
				//.GetComponent<NavMeshAgent> ().speed = 10.0f;
			}
			if(poison == true){
				t.poisonme();
				//SleepTimeout(5000);
				//.GetComponent<NavMeshAgent> ().speed = 10.0f;
			}
			t.health = t.health - damage;
			
			// call Teddy's onDead if died
			if (t.health <= 0)         
				t.onDeath();            
			
			// destroy bullet
			Destroy(gameObject);
		}
	}
	
	public void setDestination(Transform v, bool slow, bool poison) {
		destination = v;
		this.slow = slow;
		this.poison = poison;
	}
}