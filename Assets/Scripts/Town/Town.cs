using UnityEngine;
using System.Collections;

public class Town : MonoBehaviour {
	public GUISkin skin = null;
	
	public int health = 10;
	public int enemyHit;

	
	void OnGUI() {
		GUI.skin = skin;
		
		// draw castle health
		//GUI.Label(new Rect(650, 18, 400, 200), "Town Health: " + health);
	}

	void Start()
	{
		health = 10;
	}

	void Update() {
		// find all teddys
		Enemy_one[] enemies = (Enemy_one[])FindObjectsOfType(typeof(Enemy_one));
		if (enemies != null) {
			//Debug.Log("lll");
			//Debug.Log(enemies.LongLength);
			// find all teddys that are close to the castle
			for (int i = 0; i < enemies.Length; ++i) {
				//Debug.Log("mmm");
				float range = Mathf.Max(collider.bounds.size.x, collider.bounds.size.z);
				if (Vector3.Distance(transform.position, enemies[i].transform.position) <= range) {            
					// decrease castle health
					if(enemies[i] is  Enemy_boss_health)
						health = health - 10;
					else 
						health = health - 1;
					//Debug.Log("kkk");
					// destroy teddy
					Destroy(enemies[i].gameObject);
				} 
			}
		}
	}
}