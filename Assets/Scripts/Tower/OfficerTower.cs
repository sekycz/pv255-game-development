﻿using UnityEngine;
using System.Collections;

public class OfficerTower : MonoBehaviour {
	// bullet
	public Bullet bulletPrefab = null;
	public GUISkin skin = null;
	bool gui;
	int level;
	
	// interval
	public float interval;	
	float timeLeft;
	
	// attack range
	public float range;
	
	// price to build the tower
	public int buildPrice;
	public int upgradePrice;

	Vector3 temp = new Vector3(0,1.0f,0);
	
	// rotation 
	public float rotationSpeed;
	public GameObject rightHand;
	public GameObject leftHand;
	public GameObject cylinder;
	
	void Start(){
		gui = false;
		level = 1;
		
		// interval
		interval = 0.05f;
		timeLeft = 0.0f;
		
		// attack range
		range = 15.0f;
		
		// price to build the tower
		buildPrice = 12;
		upgradePrice = 6;
		
		// rotation 
		rotationSpeed = 0f;

		this.transform.position += temp;
	}
	
	Enemy_one findClosestTarget() {
		Enemy_one closest = null;
		Vector3 pos = transform.position;
		
		// find all teddys
		Enemy_one[] enemies = (Enemy_one[])FindObjectsOfType(typeof(Enemy_one));
		if (enemies != null) {
			if (enemies.Length > 0) {
				closest = enemies[0];
				for (int i = 1; i < enemies.Length; ++i) {
					float cur = Vector3.Distance(pos, enemies[i].transform.position);
					float old = Vector3.Distance(pos, closest.transform.position);
					
					if (cur < old) {
						closest = enemies[i];
					}
				}
			}
		}
		
		return closest;
	}
	
	void Update() {
		// shoot next bullet?
		if (Player.showOfficer == true) {
			renderer.enabled = true;
			rightHand.renderer.enabled = true;
			leftHand.renderer.enabled = true;
			cylinder.renderer.enabled = true;
						timeLeft -= Time.deltaTime;
						if (timeLeft <= 0.0f) {
								// find the closest target (if any)
								Enemy_one target = findClosestTarget ();
								if (target != null) {        
										// is it close enough?
										if (Vector3.Distance (transform.position, target.transform.position) <= range) {
												// spawn bullet
												GameObject g = (GameObject)Instantiate (bulletPrefab.gameObject, new Vector3 (transform.position.x, 
					                                                                            transform.position.y + 0.5f,
					                                                                            transform.position.z), Quaternion.identity);
					
					
												/*transform.position.x -1.3f, 
					                                                                       transform.position.y -0.6f,
					                                                                       transform.position.z -1.5f*/
												// get access to bullet component
												Bullet b = g.GetComponent<Bullet> ();
												if (level == 1) {
														b.damage = 0.1f;
												} else if (level == 2) {
														b.damage = 0.2f;
												}
					
												// set destination        
												b.setDestination (target.transform, false, false);
					
												// reset time
												timeLeft = interval;
										}
								}
						}
		
						// always rotate a bit (animation)
						Vector3 rot = transform.eulerAngles;
						transform.rotation = Quaternion.Euler (rot.x, rot.y + Time.deltaTime * rotationSpeed, rot.z);
				} else {
					//GetComponent(MeshRenderer).enabled =false;
			//this.gameObject.SetActive(false);
			renderer.enabled = false;
			rightHand.renderer.enabled = false;
			leftHand.renderer.enabled = false;
			cylinder.renderer.enabled = false;
				}
	}
	
	void OnGUI() {    
		if (gui) {
			/*GUI.skin = skin;
			
			// get 3d position on screen        
			Vector3 v = Camera.main.WorldToScreenPoint(transform.position);
			
			// convert to gui coordinates
			v = new Vector2(v.x, Screen.height - v.y);
			var filePath = Application.dataPath + "/Textures/DeleteRed.png";
			var bytes = System.IO.File.ReadAllBytes(filePath);
			Texture2D textureDelete = new Texture2D(40,40);
			textureDelete.LoadImage(bytes);
			
			int width = 70;
			int height = 40;
			Rect r = new Rect(v.x - width / 2 -40, v.y - height / 2, width, height);
			Rect r2 = new Rect(v.x - width / 2 +50, v.y - height / 2, width - 30, height);
			GUI.contentColor = (Color.white);
			GUI.Box(r, "UPGRADE\n" + upgradePrice + " gold");
			GUI.contentColor = (Color.red);
			GUI.Box(r2, image: textureDelete);*/
			
			// mouse not down anymore and mouse over the box? then build the tower                
			/**if (Event.current.type == EventType.MouseUp && 
			    r.Contains(Event.current.mousePosition) &&
			    Player.gold >= upgradePrice) {
				// decrease gold lalala
				Player.gold -= upgradePrice;
				
				this.gameObject.transform.localScale += new Vector3(0.5f,0.5f,0.5f);
				this.gameObject.transform.position += new Vector3(0,0.5f,0);
				
				rightHand.renderer.material.color = Color.red;
				leftHand.renderer.material.color = Color.red;
				level = 2;
				upgradePrice = 6;
			}
			if (Event.current.type == EventType.MouseUp && 
			    r2.Contains(Event.current.mousePosition)){
				// decrease gold
				//Player.gold -= kylePrefab.buildPrice;
				
				Destroy(this.gameObject);
				Player.gold = Player.gold + totalPrice/2;
			}*/
			
		}
	}
	
	public void OnMouseDown() {
		gui = true;
	}
	
	
	public void OnMouseUp() {
		gui = false;
	}
}
