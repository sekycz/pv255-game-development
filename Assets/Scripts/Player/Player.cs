﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public GUISkin skin = null;

	//starting gold
	public static  int gold;
	public static bool showOfficer;
	public static bool isPaused;

	private Town town;
	private level_one_wave level_one;
	private level_two_wave level_two;
	private level_three_wave level_three;
	private int health;

	private float widthPercent = 0.2f;
	private float heightPercent = 0.5f;
	private int currentWave;
	private float timeleft;

	private Texture2D textureRestartButton;
	private Texture2D texturePauseButton;
	private Texture2D texturePauseButtonGreen;
	private Texture2D texturePauseButtonRed;
	private Texture2D textureFastForward;

	public string Text; 
	Texture2D textureOfficer;
	Texture2D textureEarthquake;
	Texture2D textureExit;
	public OfficerTower officerPrefab = null;

	private GUIStyle guiStyle;
	

	void Start()
	{
		Text = "1x";
		gold = 20;
		showOfficer = false;
		isPaused = false;
		town = GameObject.Find ("Town").GetComponent<Town>();

		if(Application.loadedLevelName.ToString().Equals("level_one")){
			level_one = GameObject.Find ("_GameManager").GetComponent<level_one_wave>();
			health = town.health;
			currentWave = level_one.currentWave;
			timeleft = level_one.timeLeft;
		} else if(Application.loadedLevelName.ToString().Equals("level_two")){
			level_two = GameObject.Find ("_GameManager").GetComponent<level_two_wave>();
			health = town.health;
			currentWave = level_two.currentWave;
			timeleft = level_two.timeLeft;
		} else if(Application.loadedLevelName.ToString().Equals("level_three")){
			level_three = GameObject.Find ("_GameManager").GetComponent<level_three_wave>();
			health = town.health;
			currentWave = level_three.currentWave;
			timeleft = level_three.timeLeft;
		}


		//texture load
		//textureRestartButton = new Texture2D(37,37);
		textureFastForward = new Texture2D(37,37);
		texturePauseButton = new Texture2D(37,37);
		textureRestartButton = (Resources.Load("Textures/restartButton")) as Texture2D;
		texturePauseButtonGreen = (Resources.Load("Textures/pauseButtonGreen")) as Texture2D;
		texturePauseButtonRed = (Resources.Load("Textures/pauseButtonRed")) as Texture2D;
		textureFastForward =  (Resources.Load("Textures/fast-forward")) as Texture2D;

		texturePauseButton = texturePauseButtonGreen;

		//textureOfficer = new Texture2D(58,58);
		textureOfficer =  (Resources.Load("Textures/OfficerFace")) as Texture2D;
		
		//textureEarthquake = new Texture2D(58,58);
	    textureEarthquake = (Resources.Load("Textures/earthquakeIcon")) as Texture2D;

		textureExit = (Resources.Load("Textures/exit")) as Texture2D;

	}

	void Update()
	{
		health = town.health;
		if(Application.loadedLevelName.ToString().Equals("level_one")){
			currentWave = level_one.currentWave;
			timeleft = level_one.timeLeft;
		} else if(Application.loadedLevelName.ToString().Equals("level_two")){
			currentWave = level_two.currentWave;
			timeleft = level_two.timeLeft;
		} else if(Application.loadedLevelName.ToString().Equals("level_three")){
			currentWave = level_three.currentWave;
			timeleft = level_three.timeLeft;
		}
	}

	void OnGUI(){
		GUI.skin = skin;

		//GAME OVER
		//health == 0
		if(health <= 0)
		{

			Rect centerRect = new Rect (Screen.width * (1 - 0.3f) / 2,
			                          Screen.height * (1 - 0.3f) / 2,
			                          Screen.width * 0.3f,
			                          Screen.height * 0.9f);
			GUILayout.BeginArea (centerRect);
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("<size=30><color=#ff0000ff><b>GAME OVER!</b></color></size>", GUILayout.Height(Screen.height * 0.3f));

			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			Time.timeScale = 0;
			GUILayout.EndArea();

			Rect centerRectButton = new Rect (Screen.width * (1 - 0.16f) / 2, Screen.height * (1 + 0.4f) / 2,
			                                  Screen.width * 0.16f,
			                                  Screen.height * 0.08f);

//			Rect centerRectButton = new Rect (Screen.width/2-63, Screen.height/2+80,
//			                                  Screen.width * 0.16f,
//			                                  Screen.height * 0.08f);

			if (GUI.Button (centerRectButton,"<size=15><b>Restart</b></size>")) {
				Application.LoadLevel(Application.loadedLevelName);
			}


			//disable towerslots
			TowerSlot[] towers = (TowerSlot[])FindObjectsOfType(typeof(TowerSlot));
			if (towers != null)
			{
				for (int i = 0; i < towers.Length; ++i)
				{
					towers[i].isFull = true;

				}

			}
			 

		}



		//FORWARD BUTTON
		Rect forwardRect = new Rect (	 Screen.width* 0.655f,
		                             5.0f,
		                             Screen.width * 0.06f,
		                             Screen.height * 0.1f);
		if(GUI.Button (forwardRect, image: textureFastForward))
		{

			if (Time.timeScale == 1.0f)
			{
				Text = "2x";
				Time.timeScale = 2.0f;
			}
			else if (Time.timeScale == 2.0f)
			{
				Time.timeScale = 3.0f;
				Text = "3x";
			}
			else 
			{
				Time.timeScale = 1.0f;
				Text = "1x";
			}
		}
		//GUI.Label(forwardRect,Text);
		Rect speed = new Rect(	 Screen.width* 0.655f,
		                   20.5f,
		                   Screen.width * 0.06f,
		                   Screen.height * 0.1f);
		guiStyle = GUI.skin.label;
		GUI.TextArea(speed,Text,guiStyle);


		//RESTART BUTTON
		Rect restartRect = new Rect (	 Screen.width* 0.728f,
		                          5.0f,
		                          Screen.width * 0.06f,
		                          Screen.height * 0.1f);

		//GUILayout.BeginArea (restartRect);
		if(GUI.Button (restartRect, image: textureRestartButton))
		{
			Time.timeScale = 0;
			Application.LoadLevel(Application.loadedLevelName);
		}


		//PAUSE BUTTON
		Rect pauseRect = new Rect (	 Screen.width* 0.581f,
		                             5.0f,
		                             Screen.width * 0.06f,
		                             Screen.height * 0.1f);
		
		//GUILayout.BeginArea (restartRect);
		if(GUI.Button (pauseRect, image: texturePauseButton))
		{
			if (!isPaused) {
				texturePauseButton = texturePauseButtonRed;
				Time.timeScale = 0.0f;
				isPaused = true;
			} else {
				Time.timeScale = 1.0f;
				texturePauseButton = texturePauseButtonGreen;
				isPaused = false;
			}
		}


		//GUILayout.EndArea();


		//STATISTICS
		Rect statRect = new Rect (	 Screen.width* 0.795f,
		                          5.0f,
		                          Screen.width * widthPercent,
		                          Screen.height * heightPercent);

		if(Application.loadedLevelName.ToString().Equals("level_one")){
			GUILayout.BeginArea (statRect);
			GUILayout.BeginVertical ("box");//area is box
			GUILayout.Label ("<b><size=15>Gold: <color=#FFD700>" + gold+"</color></size></b>");
			GUILayout.Label("<b><size=15>Town Health: <color=#ff0000ff>" + health+"</color></size></b>");
			GUILayout.Label("<b><size=15>Waves: " + currentWave + "/"+ level_one.WaveCounts + "</size></b>");
			
			//wave labels
			if(timeleft == level_one.waitInterval)
				GUILayout.Label("<b><size=15><color=#ff0000ff>Sending wave!</color></size></b>");
			else if (currentWave != level_one.WaveCounts)
				GUILayout.Label("<b><size=15>Next wave: <color=#ff0000ff>" + timeleft.ToString("F1") + "s</color></size></b>");
			else
				GUILayout.Label("<b><size=15><color=#ff0000ff>Waves are over!</color></size></b>");
			GUILayout.EndVertical ();
			
			//wave is sending button
			if(timeleft == level_one.waitInterval)
			{
				GUILayout.Button ("<b><size=15>Sending!</size></b>");
			}
			else if (currentWave != level_one.WaveCounts)
			{
				
				if (GUILayout.Button ("<b><size=15>Next wave</size></b>")) {
					level_one.SendNextWave();
				}
			}
			else{
				GUILayout.Button ("<b><size=15>Over!</size></b>");
			}
			GUILayout.EndArea();
		} else if(Application.loadedLevelName.ToString().Equals("level_two")){
			GUILayout.BeginArea (statRect);
			GUILayout.BeginVertical ("box");//area is box
			GUILayout.Label ("<b><size=15>Gold: <color=#FFD700>" + gold+"</color></size></b>");
			GUILayout.Label("<b><size=15>Town Health: <color=#ff0000ff>" + health+"</color></size></b>");
			GUILayout.Label("<b><size=15>Waves: " + currentWave + "/"+ level_two.WaveCounts + "</size></b>");
			
			//wave labels
			if(timeleft == level_two.waitInterval)
				GUILayout.Label("<b><size=15><color=#ff0000ff>Sending wave!</color></size></b>");
			else if (currentWave != level_two.WaveCounts)
				GUILayout.Label("<b><size=15>Next wave: <color=#ff0000ff>" + timeleft.ToString("F1") + "s</color></size></b>");
			else
				GUILayout.Label("<b><size=15><color=#ff0000ff>Waves are over!</color></size></b>");
			GUILayout.EndVertical ();
			
			//wave is sending button
			if(timeleft == level_two.waitInterval)
			{
				GUILayout.Button ("<b><size=15>Sending!</size></b>");
			}
			else if (currentWave != level_two.WaveCounts)
			{
				
				if (GUILayout.Button ("<b><size=15>Next wave</size></b>")) {
					level_two.SendNextWave();
				}
			}
			else{
				GUILayout.Button ("<b><size=15>Over!</size></b>");
			}
			GUILayout.EndArea();
		} else if(Application.loadedLevelName.ToString().Equals("level_three")){
			GUILayout.BeginArea (statRect);
			GUILayout.BeginVertical ("box");//area is box
			GUILayout.Label ("<b><size=15>Gold: <color=#FFD700>" + gold+"</color></size></b>");
			GUILayout.Label("<b><size=15>Town Health: <color=#ff0000ff>" + health+"</color></size></b>");
			GUILayout.Label("<b><size=15>Waves: " + currentWave + "/"+ level_three.WaveCounts + "</size></b>");
			
			//wave labels
			if(timeleft == level_three.waitInterval)
				GUILayout.Label("<b><size=15><color=#ff0000ff>Sending wave!</color></size></b>");
			else if (currentWave != level_three.WaveCounts)
				GUILayout.Label("<b><size=15>Next wave: <color=#ff0000ff>" + timeleft.ToString("F1") + "s</color></size></b>");
			else
				GUILayout.Label("<b><size=15><color=#ff0000ff>Waves are over!</color></size></b>");
			GUILayout.EndVertical ();
			
			//wave is sending button
			if(timeleft == level_three.waitInterval)
			{
				GUILayout.Button ("<b><size=15>Sending!</size></b>");
			}
			else if (currentWave != level_three.WaveCounts)
			{
				
				if (GUILayout.Button ("<b><size=15>Next wave</size></b>")) {
					level_three.SendNextWave();
				}
			}
			else{
				GUILayout.Button ("<b><size=15>Over!</size></b>");
			}
			GUILayout.EndArea();
		}

		//show gold amount


		Rect bonusRect = new Rect (	 Screen.width - 65,
		                           Screen.height - 65,
		                           60,
		                           60);
		
		Rect bonusRect2 = new Rect (	 Screen.width - 65,
		                            Screen.height - 130,
		                            60,
		                            60);

		Rect bonusRect3 = new Rect (	  5,
		                            Screen.height - 65,
		                            60,
		                            60);
		
		//fizl bonus
		Rect rT = new Rect(Screen.width - 67,Screen.height - 22, 60, 20);
		GUI.Box(bonusRect, image: textureOfficer);
		guiStyle = GUI.skin.label;
		GUI.contentColor = Color.yellow;
		GUI.skin.textArea.normal.background = null;
		GUI.skin.textArea.active.background = null;
		GUI.skin.textArea.onHover.background = null;
		GUI.skin.textArea.hover.background = null;
		GUI.skin.textArea.onFocused.background = null;
		GUI.skin.textArea.focused.background = null;
		GUI.TextArea(rT,officerPrefab.buildPrice + " gold",guiStyle);

		// zemetreseni bonus
		Rect rT2 = new Rect(Screen.width - 67,Screen.height - 87, 60, 20);
		GUI.Box(bonusRect2, image: textureEarthquake);
		GUI.TextArea(rT2,"10 gold",guiStyle);

		//policeoffices
		if (Event.current.type == EventType.MouseDown && 
		    bonusRect.Contains(Event.current.mousePosition) &&
		    Player.gold > 11 && this.health > 0){

			showOfficer = true;
			Player.gold = Player.gold-12;
		}
		//earthquake
		if (Event.current.type == EventType.MouseUp && 
		    bonusRect2.Contains(Event.current.mousePosition) &&
		    Player.gold > 9 && this.health > 0){

			Player.gold = Player.gold - 10;
			this.gameObject.AddComponent("CameraShake");
			Enemy_one[] enemies = (Enemy_one[])FindObjectsOfType(typeof(Enemy_one));
			foreach(Enemy_one enemy in enemies){
				enemy.health = enemy.health - 2;
				if (enemy.health <= 0)
					enemy.onDeath();
			}
			
		}
		//back to menu
		if(GUI.Button (bonusRect3, image: textureExit))
		{
			Application.LoadLevel("scene_menu");
		}
	}

}
