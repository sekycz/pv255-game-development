﻿using UnityEngine;
using System.Collections;

public class Enemy_flyer : Enemy_one {


	public float speed;
	public GameObject body;

	bool[] waypoints;
	// Use this for initialization
	void Start () {
		maxHealth = 10;
		health = 10;
		waypoints = new bool[]{true, false , false, false, false}	;
		speed = 5.0f;
		defaultSpeed = 5.0f;
		this.flyer = true;
	}


	void Update()
	{
		this.currentHealthbarWidth = (healthbarWidth * health) / maxHealth;
		slowTime -= Time.deltaTime;
		poisonTime -= Time.deltaTime;
		if (poisonTime < 0 && slowTime < 0) {
			body.renderer.material.color = Color.white;
		}
		if (slowTime < 0 && poisonTime >= 0) {
			this.health = this.health - 0.03f;
			body.renderer.material.color = Color.green;
			if (this.health <= 0)         
				this.onDeath(); 
		}
		
		if (poisonTime < 0 && slowTime >= 0) {
			body.renderer.material.color = Color.Lerp(Color.blue,Color.cyan,0.4f);
			this.speed = 1.0f;
		}else {
			this.speed = defaultSpeed;
		}
		if (poisonTime >= 0 && slowTime >= 0) {
			this.speed = 1.0f;
			this.health = this.health - 0.03f;
			if (this.health <= 0)         
				this.onDeath(); 
		}
		float step = speed * Time.deltaTime;

		float range = Mathf.Max(collider.bounds.size.x, collider.bounds.size.z);

		if(waypoints[0])
			this.gameObject.transform.position = Vector3.MoveTowards (this.transform.position, GameObject.Find ("Waypoint1").transform.position, step);
		else if(waypoints[1])
			this.gameObject.transform.position = Vector3.MoveTowards (this.transform.position, GameObject.Find ("Waypoint2").transform.position, step);
		else if(waypoints[2])
			this.gameObject.transform.position = Vector3.MoveTowards (this.transform.position, GameObject.Find ("Waypoint3").transform.position, step);
		else if(waypoints[3])
			this.gameObject.transform.position = Vector3.MoveTowards (this.transform.position, GameObject.Find ("Waypoint4").transform.position, step);
		else if(waypoints[4])
			this.gameObject.transform.position = Vector3.MoveTowards (this.transform.position, GameObject.Find ("Waypoint5").transform.position, step);

		if (Vector3.Distance (this.transform.position, GameObject.Find ("Waypoint1").transform.position) <= range)
		{
			waypoints[0] = false;
			waypoints[1] = true;
		}
		else if (Vector3.Distance (this.transform.position, GameObject.Find ("Waypoint2").transform.position) <= range)
		{
			waypoints[1] = false;
			waypoints[2] = true;
		}
		else if (Vector3.Distance (this.transform.position, GameObject.Find ("Waypoint3").transform.position) <= range)
		{
			waypoints[2] = false;
			waypoints[3] = true;
		}
		else if (Vector3.Distance (this.transform.position, GameObject.Find ("Waypoint4").transform.position) <= range)
		{
			waypoints[3] = false;
			waypoints[4] = true;
		}
		/*else if (Vector3.Distance (this.transform.position, GameObject.Find ("Waypoint5").transform.position) <= range)
		{
			waypoints[4] = false;
			waypoints[2] = true;
		}*/


	}

}
