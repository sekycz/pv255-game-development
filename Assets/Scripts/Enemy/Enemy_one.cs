﻿using UnityEngine;
using System.Collections;

public class Enemy_one : MonoBehaviour {

	public GameObject enemy;
	public Texture2D blackBackground;
	public Texture2D whiteBackground;
	public Texture2D healthTexture;

	//current health
	[System.NonSerialized]
	public float health = 12;
	public bool lockDeath;

	protected float maxHealth = 12;

	protected float currentHealthbarWidth;
	protected float healthbarWidth = 30f;
	private bool draw = true;

	private Camera camera = Camera.main;

	private bool gameSuccess;
	public bool flyer = false;
	public float slowTime = 0.0f;
	public float poisonTime = 0.0f;
	public float defaultSpeed = 3.5f;

	void Start(){
		gameSuccess = false;
		lockDeath = false;
		this.gameObject.GetComponent<NavMeshAgent> ().obstacleAvoidanceType = 0; //dont collide with others
		this.gameObject.GetComponent<NavMeshAgent> ().updateRotation = false;
	}
		
	// Update is called once per frame
	void Update () {

		/*TextMesh tm = GetComponentInChildren<TextMesh> ();
		tm.text = new string ('-', health);
		tm.renderer.material.color = Color.red;*/

		//gotexture = GameObject.Find ("Enemy1");
		//Texture2D mytexture =(Texture2D) Resources.Load("Textures/KennyMcCormick");
		//gotexture.renderer.material.mainTexture = mytexture;
		//enemy.renderer.material.color = Color.black;


		//health bar face camera position
		//tm.transform.forward = Camera.main.transform.forward;
		this.currentHealthbarWidth = (healthbarWidth * health) / maxHealth;
		slowTime -= Time.deltaTime;
		poisonTime -= Time.deltaTime;
		if (poisonTime < 0 && slowTime < 0) {
			this.gameObject.renderer.material.color = Color.white;
		}
		if (slowTime < 0 && poisonTime >= 0) {
			this.health = this.health - 0.03f;
			this.gameObject.renderer.material.color = Color.green;
			if (this.health <= 0)         
				this.onDeath(); 
		}

		if (poisonTime < 0 && slowTime >= 0) {
			this.gameObject.renderer.material.color = Color.Lerp(Color.blue,Color.cyan,0.7f);
			this.gameObject.GetComponent<NavMeshAgent> ().speed = 1.0f;
		}else {
			this.gameObject.GetComponent<NavMeshAgent> ().speed = defaultSpeed;
		}
		if (poisonTime >= 0 && slowTime >= 0) {
			this.gameObject.GetComponent<NavMeshAgent> ().speed = 1.0f;
			this.health = this.health - 0.03f;
			if (this.health <= 0)         
				this.onDeath(); 
		}
	}

	public void onDeath() {

		if(lockDeath == false){
			lockDeath = true;
			//increase player gold
			if(Time.timeScale != 0)
				Player.gold = Player.gold + 1;

			//check for boss UARGH!
			if(this is  Enemy_boss_health)
			{
				Player.gold = Player.gold + 5;
				gameSuccess = true;
				//this.renderer.enabled = false;//hide her ass
			}
			else
			//remove object from scene
			Destroy (gameObject);
		}

	}

	public void OnGUI()
	{



		//boss killed
		if(gameSuccess)
		{

			Rect centerRect = new Rect (Screen.width * (1 - 0.3f) / 2,
			                            Screen.height * (1 - 0.3f) / 2,
			                            Screen.width * 0.3f,
			                            Screen.height * 0.9f);
			GUILayout.BeginArea (centerRect);
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label("<size=28><color=#66CD00><b>YOU WIN THIS ROUND!</b></color></size>", GUILayout.Height(Screen.height * 0.2f));
			
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
			
			Rect centerRectButton = new Rect (Screen.width * (1 - 0.16f) / 2, Screen.height * (1 + 0.2f) / 2,
			                                  Screen.width * 0.16f,
			                                  Screen.height * 0.08f);
			
			if (GUI.Button (centerRectButton,"<size=15><b>Back to menu</b></size>")) {
				Application.LoadLevel("scene_menu");
			}

			Time.timeScale = 0.0f;
		}

		if (!draw)
			return;



		Vector3 screenPosition = camera.WorldToScreenPoint(transform.position);// gets screen position.
		screenPosition.y = Screen.height - (screenPosition.y + 1);// inverts y
		GUI.DrawTexture (new Rect (screenPosition.x - 24, screenPosition.y - 24, this.healthbarWidth+2 , 10), blackBackground);
		GUI.DrawTexture (new Rect (screenPosition.x - 23, screenPosition.y - 23, this.healthbarWidth , 8), whiteBackground);
	    GUI.DrawTexture (new Rect (screenPosition.x - 23, screenPosition.y - 23, this.currentHealthbarWidth , 8), healthTexture);


	
		
		
	}

	public void OnBecameVisible()
	{
		draw = true;
	}

	public void OnBecameInvisible()
	{
		draw = false;
	}

	public void slowme(){
		this.slowTime = 5.0f;
	}

	public void poisonme(){
		this.poisonTime = 5.0f;
	}
}
