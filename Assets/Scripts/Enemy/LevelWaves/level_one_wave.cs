﻿using UnityEngine;
using System.Collections;

public class level_one_wave : MonoBehaviour {


	public int enemy1_count;
	public int enemy1_count2;
	public int enemyFlyer_count;
	public int enemyFast_count;
	public int enemyBighHealth_count;
	public int enemyBoss_count;

	public int currentWave = 0;
	public int WaveCounts = 6;

	//enemy prefabs
	public  GameObject enemy1;
	public  GameObject enemy_flyer;
	public  GameObject enemy_fast;
	public  GameObject enemy_bigHealth;
	public  GameObject enemy_boss;

	private Spawn spawn;

	//wave trigger true = not send yet/   false = already sent
	private bool[] waves;

	private GameObject currentEnemy;



	//counter wait time
	public float waitInterval = 10.0f;
	public float timeLeft;
	private bool sendNext = false;



	// Use this for initialization
	void Start () {
		if(Time.timeScale == 0)
		{
			Time.timeScale = 1.0f;
			Player.gold = 20;
		}
		WaveCounts = 6;

		enemy1_count = 8;
		enemyFast_count = 7;
		enemy1_count2 = 10;
		enemyFlyer_count = 8;
		enemyBighHealth_count = 12;
		enemyBoss_count = 1;

		timeLeft = waitInterval;
		waves = new bool[WaveCounts];

		spawn = GameObject.Find ("Start").GetComponent<Spawn>();//.GetComponent (Spawn);
		//initialize prefabs

		//enemy1 = (GameObject)Resources.LoadAssetAtPath("Assets/Prefabs/Token.prefab", typeof(GameObject));
		//enemy_fast = (GameObject)Resources.LoadAssetAtPath("Assets/Prefabs/Craig.prefab", typeof(GameObject));
		//enemy_bigHealth = (GameObject)Resources.LoadAssetAtPath("Assets/Prefabs/Tweek.prefab", typeof(GameObject));
		//enemy_boss = (GameObject)Resources.LoadAssetAtPath("Assets/Prefabs/Sheila.prefab", typeof(GameObject));
		//initialize waves
		for (int i = 0; i < WaveCounts; i++)
			waves[i] = true;
		currentWave = 0;
		WavesSend();
	}

	// Update is called once per frame
	void Update () {

		if (sendNext)
		{
			timeLeft -= Time.deltaTime;
			if (timeLeft <= 0.0f) {
				WavesSend();
				timeLeft = waitInterval;
				sendNext = false;
			}
			
		}
		else
		//current wave is still sending
		if (spawn.spawnUnitsLeft == 0 && currentWave != WaveCounts && waves[currentWave] == true)
		{
			waves[currentWave] = false;
			currentWave++;
			//send next wave
			sendNext = true;

		
				
		}


				
	}

	public void SendNextWave()
	{
		this.timeLeft = 0.01f;
	}



	void WavesSend()
	{
		//first wave
		if(waves[0] == true)
		{
			currentEnemy = enemy1;
			spawn.enemy = currentEnemy;
			spawn.spawnUnitsLeft = enemy1_count;
			spawn.interval = 1.5f;
		}
		//second wave
		else if(waves[1] == true)
		{
			currentEnemy = enemy_fast;
			spawn.enemy = currentEnemy;
			spawn.spawnUnitsLeft = enemyFast_count;
			spawn.interval = 1.5f;
		}
		//second wave
		else if(waves[2] == true)
		{
			currentEnemy = enemy1;
			spawn.enemy = currentEnemy;
			spawn.spawnUnitsLeft = enemy1_count2;
			spawn.interval = 0.7f;
		}
		else if(waves[3] == true)
		{
			currentEnemy = enemy_flyer;
			spawn.enemy = currentEnemy;
			spawn.spawnUnitsLeft = enemyFlyer_count;
			spawn.interval = 1.5f;
		}
		else if(waves[4] == true)
		{
			currentEnemy = enemy_bigHealth;
			spawn.enemy = currentEnemy;
			spawn.spawnUnitsLeft = enemyBighHealth_count;
			spawn.interval = 2.0f;
		}
		else if(waves[5] == true)
		{
			currentEnemy = enemy_boss;
			spawn.enemy = currentEnemy;
			spawn.spawnUnitsLeft = enemyBoss_count;
			spawn.interval = 2.0f;
		}

	}

	

}
