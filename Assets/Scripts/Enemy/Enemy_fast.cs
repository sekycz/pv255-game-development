using UnityEngine;
using System.Collections;

public class Enemy_fast : Enemy_one {
	
	// Use this for initialization
	void Start () {

		this.gameObject.GetComponent<NavMeshAgent> ().speed = 5.0f;
		this.gameObject.GetComponent<NavMeshAgent> ().angularSpeed = 150f;
		this.gameObject.GetComponent<NavMeshAgent> ().acceleration = 10.0f;


		this.gameObject.GetComponent<NavMeshAgent> ().updateRotation = false;
		//ignore others
		this.gameObject.GetComponent<NavMeshAgent> ().obstacleAvoidanceType = 0; //None
		this.defaultSpeed = 9.0f;

	
	}
	
}
