﻿using UnityEngine;
using System.Collections;

public class Enemy_boss_health : Enemy_one {

	void Start () {
		maxHealth = 400;
		health = 400;
		defaultSpeed = 3.0f;

		this.gameObject.GetComponent<NavMeshAgent> ().speed = 3.0f;
		this.gameObject.GetComponent<NavMeshAgent> ().angularSpeed = 120f;
		this.gameObject.GetComponent<NavMeshAgent> ().acceleration = 8.0f;

		this.gameObject.GetComponent<NavMeshAgent> ().updateRotation = false;
		//ignore others
		this.gameObject.GetComponent<NavMeshAgent> ().obstacleAvoidanceType = 0; //None
	
	}
	

}
