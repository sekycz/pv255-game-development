﻿using UnityEngine;
using System.Collections;

public class Enemy_bigHealth : Enemy_one {


	// Use this for initialization
	void Start () {
		maxHealth = 25;
		health = 25;
		defaultSpeed = 3.5f;

		this.gameObject.GetComponent<NavMeshAgent> ().updateRotation = false;
		//ignore others
		this.gameObject.GetComponent<NavMeshAgent> ().obstacleAvoidanceType = 0; //None
	
	}

}
