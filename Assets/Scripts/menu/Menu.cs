﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {
	public GUISkin skin = null;

	//centeting parameters
	public float widthPercent = 0.3f;
	public float heightPercent = 0.3f;
	public Texture2D background;

	void OnGUI(){
		//default skin if thers is nothing
		GUI.skin = skin;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), background);

		/**
		 * Menu rectangle adjusting to screen resolution in the center
		 **/
		Rect menuRect = new Rect (Screen.width * (1 - widthPercent) / 2,
		                         Screen.height * (1 - heightPercent) / 2,
		                         Screen.width * 0.3f,
		                         Screen.height * 0.3f);

		//drawing menu
		GUILayout.BeginArea (menuRect);
		GUILayout.BeginVertical ("box");//area is box
			//click event
			if (GUILayout.Button ("Level 1")) {
				Application.LoadLevel("level_one");								
			}	
			if (GUILayout.Button ("Level 2")) {
				Application.LoadLevel("level_two");								
			}
			if (GUILayout.Button ("Level 3")) {
				Application.LoadLevel("level_three");								
			}
			//click event
			if (GUILayout.Button ("Quit")) {
				Application.Quit();								
			}	
		GUILayout.EndVertical ();
		GUILayout.EndArea ();



		}


}
