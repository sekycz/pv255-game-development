﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
	//spawning interval
	public float interval = 6.0f;
	public int spawnUnitsLeft = 5;

	float timeLeft = 0.0f;

	//object to be spawned
	public GameObject enemy = null;

	//destination of enemy way
	public Transform destination = null;


	// Update is called once per frame
	void Update () {
	
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0.0f) {
			//spawning
			if(spawnUnitsLeft > 0)
			{
				spawnUnitsLeft--;

				GameObject gObj = (GameObject)Instantiate(enemy, transform.position, Quaternion.Euler(transform.rotation.x, transform.rotation.y - 75.0f, transform.rotation.z));

				//navmesh agent
				NavMeshAgent ag = gObj.GetComponent<NavMeshAgent>();
				//check for flyers, they dont use navmesh
				if(ag.enabled != false && ag != null)
					ag.destination = destination.position;
				
			//reset time
			timeLeft = interval;
			}
		}
				
	
	}
}
