﻿using UnityEngine;
using System.Collections;

public class TowerSlot : MonoBehaviour {
	public GUISkin skin = null;
	
	bool gui = false;
	public bool isFull = false;

	// Tower prefab
	public CartmanTower cartmanPrefab = null;
	public KyleTower kylePrefab = null;
	public StanTower stanPrefab = null;
	public KennyTower kennyPrefab = null;

	Texture2D textureCartman;
	Texture2D textureKyle;
	Texture2D textureStan;
	Texture2D textureKenny;

	void Start(){

		/*textureCartman = new Texture2D(37,37);
		textureKyle = new Texture2D(37,37);
		textureStan = new Texture2D(37,37);
		textureKenny = new Texture2D(37,37);*/

	    textureCartman = (Resources.Load("Textures/CartmannFace")) as Texture2D;

		textureKyle = (Resources.Load("Textures/KyleFace")) as Texture2D;

		textureStan = (Resources.Load("Textures/FaceStanley")) as Texture2D;

		textureKenny = (Resources.Load("Textures/KennyFace")) as Texture2D;

		this.isFull = false;
	}
	
	void OnGUI() {    
		if (gui && !isFull) {
			GUI.skin = skin;
			
			// get 3d position on screen        
			Vector3 v = Camera.main.WorldToScreenPoint(transform.position);
			
			// convert to gui coordinates
			v = new Vector2(v.x, Screen.height - v.y);

			
			// creation menu for tower
			int width = 60;
			int height = 63;
			Rect r = new Rect(v.x - width / 2 -62, v.y - height / 2, width, height);
			Rect rT = new Rect(v.x - width / 2 -52, v.y - height / 2 + 43, 50, 25);
			Rect r2 = new Rect(v.x - width / 2 +62, v.y - height / 2, width, height);
			Rect r2T = new Rect(v.x - width / 2 +72, v.y - height / 2 + 43, 50, 25);
			Rect r3 = new Rect(v.x - width / 2, v.y - height / 2 -50, width, height+7);
			Rect r3T = new Rect(v.x - width / 2 + 9, v.y - height / 2 +1, 50, 25);
			Rect r4 = new Rect(v.x - width / 2, v.y - height / 2 +50, width, height+4);
			Rect r4T = new Rect(v.x - width / 2 + 9, v.y - height / 2 +97, 50, 25);
			GUI.skin.textArea.normal.background = null;
			GUI.skin.textArea.active.background = null;
			GUI.skin.textArea.onHover.background = null;
			GUI.skin.textArea.hover.background = null;
			GUI.skin.textArea.onFocused.background = null;
			GUI.skin.textArea.focused.background = null;
			GUI.contentColor = (Player.gold >= cartmanPrefab.buildPrice ? Color.white : Color.red);
			GUI.Box(r, image: textureCartman);
			GUI.contentColor = (Player.gold >= kylePrefab.buildPrice ? Color.white : Color.red);
			GUI.Box(r2, image: textureKyle);
			GUI.contentColor = Color.yellow;
			GUI.contentColor = (Player.gold >= stanPrefab.buildPrice ? Color.white : Color.red);
			GUI.Box(r3, image: textureStan);
			GUI.contentColor = (Player.gold >= kennyPrefab.buildPrice ? Color.white : Color.red);
			GUI.Box(r4, image: textureKenny);
			GUI.contentColor = Color.yellow;
			GUI.TextArea(rT,cartmanPrefab.buildPrice + " gold");
			GUI.TextArea(r2T,kylePrefab.buildPrice + " gold");
			GUI.TextArea(r3T,stanPrefab.buildPrice + " gold");
			GUI.TextArea(r4T,kennyPrefab.buildPrice + " gold");
			
			// mouse not down anymore and mouse over the box? then build the tower                
			if (Event.current.type == EventType.MouseUp && 
			    r.Contains(Event.current.mousePosition) &&
			    Player.gold >= cartmanPrefab.buildPrice) {
				// decrease gold lalala
				Player.gold -= cartmanPrefab.buildPrice;

				CartmanTower obj = (CartmanTower)Instantiate(cartmanPrefab, new Vector3(transform.position.x , 
				                                    transform.position.y +1.1f,
				                                    transform.position.z ), Quaternion.identity);

				obj.towerSlot = this;

				isFull = true;

			}
			if (Event.current.type == EventType.MouseUp && 
			    r2.Contains(Event.current.mousePosition) &&
			    Player.gold >= kylePrefab.buildPrice) {
				// decrease gold
				Player.gold -= kylePrefab.buildPrice;

				KyleTower  obj = (KyleTower)Instantiate(kylePrefab, new Vector3(transform.position.x , 
				                                       transform.position.y +1.1f,
				                                       transform.position.z ), Quaternion.identity);
				obj.towerSlot = this;
				isFull = true;
			}
			if (Event.current.type == EventType.MouseUp && 
			    r3.Contains(Event.current.mousePosition) &&
			    Player.gold >= stanPrefab.buildPrice) {
				// decrease gold
				Player.gold -= stanPrefab.buildPrice;

				StanTower obj = (StanTower)Instantiate(stanPrefab, new Vector3(transform.position.x , 
				                                       transform.position.y +1.1f,
				                                       transform.position.z ), Quaternion.identity);
				obj.towerSlot = this;
				isFull = true;
			}
			if (Event.current.type == EventType.MouseUp && 
			    r4.Contains(Event.current.mousePosition) &&
			    Player.gold >= kennyPrefab.buildPrice) {
				// decrease gold
				Player.gold -= kennyPrefab.buildPrice;

				KennyTower obj = (KennyTower)Instantiate(kennyPrefab, new Vector3(transform.position.x , 
				                                       transform.position.y +1.1f,
				                                       transform.position.z ), Quaternion.identity);
				obj.towerSlot = this;
				isFull = true;
			}
		}
	}
	
	public void OnMouseDown() {
		gui = true;
	}
	
	
	public void OnMouseUp() {
		gui = false;
	}
}